# Installation instructions

## Deployment
- Extract the archive and put it in the folder you want
- Run `cp .env.example .env` file to copy example file to .env. Then edit your .env file with DB credentials and other settings.
- Run `composer install` command
- Run `php artisan migrate --seed` command. Notice: seed is important, because it will create the first admin user for you.
- Run `php artisan key:generate` command.
- If you have file/photo fields, run `php artisan storage:link` command.

And that's it, go to your [domain](http://127.0.0.1:8000) and login.

## Default credentials
**Username**: `admin@admin.com`

**Password**: `password`

## Seed Users table
if you run the seeder using 

`php artisan db:seed --class=UserSeeder`,

you will be prompted with the question 
> “How many users you want to create?”.

You can then provide the number of rows you want to seed 
or just press enter which will generate 100 rows by default.

