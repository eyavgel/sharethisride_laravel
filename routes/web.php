<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::post('users/media', 'UsersController@storeMedia')->name('users.storeMedia');
    Route::post('users/ckmedia', 'UsersController@storeCKEditorImages')->name('users.storeCKEditorImages');
    Route::resource('users', 'UsersController');

    // User Search Options
    Route::delete('user-search-options/destroy', 'UserSearchOptionController@massDestroy')->name('user-search-options.massDestroy');
    Route::resource('user-search-options', 'UserSearchOptionController');

    // Bank Accounts
    Route::delete('bank-accounts/destroy', 'BankAccountController@massDestroy')->name('bank-accounts.massDestroy');
    Route::resource('bank-accounts', 'BankAccountController');

    // Payouts
    Route::delete('payouts/destroy', 'PayoutController@massDestroy')->name('payouts.massDestroy');
    Route::resource('payouts', 'PayoutController');

    // Credit Cards
    Route::delete('credit-cards/destroy', 'CreditCardController@massDestroy')->name('credit-cards.massDestroy');
    Route::resource('credit-cards', 'CreditCardController');

    // Alerts
    Route::delete('alerts/destroy', 'AlertController@massDestroy')->name('alerts.massDestroy');
    Route::resource('alerts', 'AlertController');

    // Cars
    Route::delete('cars/destroy', 'CarController@massDestroy')->name('cars.massDestroy');
    Route::resource('cars', 'CarController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
