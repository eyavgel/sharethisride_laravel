<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:sanctum']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::post('users/media', 'UsersApiController@storeMedia')->name('users.storeMedia');
    Route::apiResource('users', 'UsersApiController');

    // User Search Options
    Route::apiResource('user-search-options', 'UserSearchOptionApiController');

    // Bank Accounts
    Route::apiResource('bank-accounts', 'BankAccountApiController');

    // Payouts
    Route::apiResource('payouts', 'PayoutApiController');

    // Credit Cards
    Route::apiResource('credit-cards', 'CreditCardApiController');

    // Alerts
    Route::apiResource('alerts', 'AlertApiController');

    // Cars
    Route::apiResource('cars', 'CarApiController');
});
