<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name'               => $this->faker->firstName,
            'last_name'          => $this->faker->lastName,
            'email'              => $this->faker->unique()->safeEmail,
            'email_verified_at'  => now(),
            'password'           => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token'     => Str::random(10),
            'gender'             => $this->faker->randomElement(['male', 'female']),
            'address_1'          => $this->faker->address,
            'address_2'          => $this->faker->secondaryAddress,
            'county'             => $this->faker->countryCode,
            'zip_code'           => $this->faker->postcode,
            'city'               => $this->faker->city,
            'country'            => $this->faker->country,
            'phone_number'       => $this->faker->phoneNumber,
            'phone_verified_at'  => now(),
            'about'              => $this->faker->text,
            'occupation'         => $this->faker->jobTitle,
            'last_activity'      => now(),
            'is_subscribed'      => $this->faker->boolean(),
            'balance'            => $this->faker->randomDigit,
            'email_options'      => $this->faker->paragraph,
            'banned_at'          => $this->faker->time(),
            'suspended'          => $this->faker->boolean(),
            'positive_passenger' => $this->faker->randomDigit,
            'negative_passenger' => $this->faker->randomDigit,
            'positive_driver'    => $this->faker->randomDigit,
            'negative_driver'    => $this->faker->randomDigit,
            'uuid'               => $this->faker->uuid,
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return Factory
     */
    public function unverified(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
