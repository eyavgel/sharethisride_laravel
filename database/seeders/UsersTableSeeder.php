<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => bcrypt('password'),
                'remember_token' => null,
                'last_name'      => '',
                'address_1'      => '',
                'address_2'      => '',
                'county'         => '',
                'zip_code'       => '',
                'city'           => '',
                'country'        => '',
                'phone_number'   => '',
                'occupation'     => '',
                'uuid'           => '',
            ],
        ];

        User::insert($users);

        $count = $this->command->ask(
            'How many users you want to create?',
            100
        );

        User::factory((int)$count)->create();
    }
}
