<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'user_search_option_create',
            ],
            [
                'id'    => 18,
                'title' => 'user_search_option_edit',
            ],
            [
                'id'    => 19,
                'title' => 'user_search_option_show',
            ],
            [
                'id'    => 20,
                'title' => 'user_search_option_delete',
            ],
            [
                'id'    => 21,
                'title' => 'user_search_option_access',
            ],
            [
                'id'    => 22,
                'title' => 'bank_account_create',
            ],
            [
                'id'    => 23,
                'title' => 'bank_account_edit',
            ],
            [
                'id'    => 24,
                'title' => 'bank_account_show',
            ],
            [
                'id'    => 25,
                'title' => 'bank_account_delete',
            ],
            [
                'id'    => 26,
                'title' => 'bank_account_access',
            ],
            [
                'id'    => 27,
                'title' => 'payout_create',
            ],
            [
                'id'    => 28,
                'title' => 'payout_edit',
            ],
            [
                'id'    => 29,
                'title' => 'payout_show',
            ],
            [
                'id'    => 30,
                'title' => 'payout_delete',
            ],
            [
                'id'    => 31,
                'title' => 'payout_access',
            ],
            [
                'id'    => 32,
                'title' => 'credit_card_create',
            ],
            [
                'id'    => 33,
                'title' => 'credit_card_edit',
            ],
            [
                'id'    => 34,
                'title' => 'credit_card_show',
            ],
            [
                'id'    => 35,
                'title' => 'credit_card_delete',
            ],
            [
                'id'    => 36,
                'title' => 'credit_card_access',
            ],
            [
                'id'    => 37,
                'title' => 'alert_create',
            ],
            [
                'id'    => 38,
                'title' => 'alert_edit',
            ],
            [
                'id'    => 39,
                'title' => 'alert_show',
            ],
            [
                'id'    => 40,
                'title' => 'alert_delete',
            ],
            [
                'id'    => 41,
                'title' => 'alert_access',
            ],
            [
                'id'    => 42,
                'title' => 'car_create',
            ],
            [
                'id'    => 43,
                'title' => 'car_edit',
            ],
            [
                'id'    => 44,
                'title' => 'car_show',
            ],
            [
                'id'    => 45,
                'title' => 'car_delete',
            ],
            [
                'id'    => 46,
                'title' => 'car_access',
            ],
            [
                'id'    => 47,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
