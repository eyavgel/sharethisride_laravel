<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertsTable extends Migration
{
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('from_point');
            $table->string('to_point');
            $table->date('leaving_on')->nullable();
            $table->time('leaving_at')->nullable();
            $table->integer('seats');
            $table->decimal('price', 15, 2)->nullable();
            $table->string('bike_rack');
            $table->string('engine_types')->nullable();
            $table->string('allowed_smoking');
            $table->string('allowed_drinks');
            $table->string('allowed_food');
            $table->string('allowed_kids');
            $table->string('allowed_pets');
            $table->string('allowed_3_backseat');
            $table->string('allowed_same_gender_ride');
            $table->integer('luggage_space');
            $table->string('allowed_music');
            $table->string('allowed_conversation');
            $table->string('partial_ride');
            $table->integer('radius_from');
            $table->integer('radius_to');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
