<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccountsTable extends Migration
{
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number');
            $table->string('bank_name');
            $table->string('account');
            $table->string('name')->nullable();
            $table->string('is_default');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
