<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSearchOptionsTable extends Migration
{
    public function up()
    {
        Schema::create('user_search_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bike_rack');
            $table->string('allowed_smoking');
            $table->string('allowed_drinks');
            $table->string('allowed_food');
            $table->string('allowed_kids');
            $table->string('allowed_pets');
            $table->string('allowed_3_backseat');
            $table->string('allowed_same_gender_ride');
            $table->integer('luggage_space');
            $table->string('allowed_music');
            $table->string('allowed_conversation');
            $table->string('engine_types')->nullable();
            $table->string('partial_ride');
            $table->integer('radius_from');
            $table->integer('radius_to');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
