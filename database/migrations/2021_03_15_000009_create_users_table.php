<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable()->unique();
            $table->datetime('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('remember_token')->nullable();
            $table->string('last_name')->nullable();
            $table->string('gender')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('county')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('phone_number')->nullable();
            $table->time('phone_verified_at')->nullable();
            $table->longText('about')->nullable();
            $table->string('occupation')->nullable();
            $table->datetime('last_activity')->nullable();
            $table->string('is_subscribed')->nullable();
            $table->decimal('balance', 15, 2)->nullable();
            $table->longText('email_options')->nullable();
            $table->time('banned_at')->nullable();
            $table->string('suspended')->nullable();
            $table->integer('positive_passenger')->nullable();
            $table->integer('negative_passenger')->nullable();
            $table->integer('positive_driver')->nullable();
            $table->integer('negative_driver')->nullable();
            $table->string('uuid')->nullable()->unique();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
