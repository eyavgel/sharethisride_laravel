<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPayoutsTable extends Migration
{
    public function up()
    {
        Schema::table('payouts', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id', 'user_fk_3441483')->references('id')->on('users');
            $table->unsignedBigInteger('bank_account_id');
            $table->foreign('bank_account_id', 'bank_account_fk_3441484')->references('id')->on('bank_accounts');
        });
    }
}
