<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('manufacturer');
            $table->string('model');
            $table->integer('seats_number');
            $table->date('production_year');
            $table->string('engine');
            $table->string('is_default');
            $table->string('is_visible');
            $table->integer('position');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
