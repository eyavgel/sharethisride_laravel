<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Alert extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'alerts';

    const BIKE_RACK_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_FOOD_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_KIDS_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_PETS_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const PARTIAL_RIDE_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_MUSIC_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_DRINKS_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_SMOKING_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_3_BACKSEAT_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_CONVERSATION_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_SAME_GENDER_RIDE_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    protected $dates = [
        'leaving_on',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'from_point',
        'to_point',
        'leaving_on',
        'leaving_at',
        'seats',
        'price',
        'bike_rack',
        'engine_types',
        'allowed_smoking',
        'allowed_drinks',
        'allowed_food',
        'allowed_kids',
        'allowed_pets',
        'allowed_3_backseat',
        'allowed_same_gender_ride',
        'luggage_space',
        'allowed_music',
        'allowed_conversation',
        'partial_ride',
        'radius_from',
        'radius_to',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'bike_rack'                => 'boolean',
        'allowed_smoking'          => 'boolean',
        'allowed_drinks'           => 'boolean',
        'allowed_food'             => 'boolean',
        'allowed_kids'             => 'boolean',
        'allowed_pets'             => 'boolean',
        'allowed_3_backseat'       => 'boolean',
        'allowed_same_gender_ride' => 'boolean',
        'allowed_conversation'     => 'boolean',
        'partial_ride'             => 'boolean',
        'allowed_music'            => 'boolean',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getLeavingOnAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setLeavingOnAttribute($value)
    {
        $this->attributes['leaving_on'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}
