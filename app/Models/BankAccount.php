<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class BankAccount extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'bank_accounts';

    const IS_DEFAULT_RADIO = [
        true  => 'True',
        false => 'False',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'number',
        'bank_name',
        'account',
        'name',
        'is_default',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'is_default' => 'boolean',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
