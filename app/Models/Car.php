<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Car extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'cars';

    const IS_DEFAULT_RADIO = [
        true  => 'True',
        false => 'False',
    ];

    const IS_VISIBLE_RADIO = [
        true  => 'Visible',
        false => 'Non visible',
    ];

    protected $dates = [
        'production_year',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'manufacturer',
        'model',
        'seats_number',
        'production_year',
        'engine',
        'is_default',
        'is_visible',
        'position',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'is_default' => 'boolean',
        'is_visible' => 'boolean',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getProductionYearAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setProductionYearAttribute($value)
    {
        $this->attributes['production_year'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }
}
