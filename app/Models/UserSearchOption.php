<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class UserSearchOption extends Model
{
    use SoftDeletes, HasFactory;

    public $table = 'user_search_options';

    const BIKE_RACK_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_FOOD_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_KIDS_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_PETS_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const PARTIAL_RIDE_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_MUSIC_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_DRINKS_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_SMOKING_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const ALLOWED_3_BACKSEAT_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_CONVERSATION_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    const ALLOWED_SAME_GENDER_RIDE_RADIO = [
        true  => 'Yes',
        false => 'No',
    ];

    protected $fillable = [
        'user_id',
        'bike_rack',
        'allowed_smoking',
        'allowed_drinks',
        'allowed_food',
        'allowed_kids',
        'allowed_pets',
        'allowed_3_backseat',
        'allowed_same_gender_ride',
        'luggage_space',
        'allowed_music',
        'allowed_conversation',
        'engine_types',
        'partial_ride',
        'radius_from',
        'radius_to',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'bike_rack'                => 'boolean',
        'allowed_smoking'          => 'boolean',
        'allowed_drinks'           => 'boolean',
        'allowed_food'             => 'boolean',
        'allowed_kids'             => 'boolean',
        'allowed_pets'             => 'boolean',
        'allowed_3_backseat'       => 'boolean',
        'allowed_same_gender_ride' => 'boolean',
        'allowed_conversation'     => 'boolean',
        'partial_ride'             => 'boolean',
        'allowed_music'            => 'boolean',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
