<?php

namespace App\Http\Requests;

use App\Models\Car;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCarRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('car_create');
    }

    public function rules()
    {
        return [
            'user_id'         => [
                'required',
                'integer',
            ],
            'manufacturer'    => [
                'string',
                'required',
            ],
            'model'           => [
                'string',
                'required',
            ],
            'seats_number'    => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'production_year' => [
                'required',
                'date_format:' . config('panel.date_format'),
            ],
            'engine'          => [
                'string',
                'required',
            ],
            'is_default'      => [
                'required',
            ],
            'is_visible'      => [
                'required',
            ],
            'position'        => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
