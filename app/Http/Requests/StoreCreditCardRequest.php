<?php

namespace App\Http\Requests;

use App\Models\CreditCard;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCreditCardRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('credit_card_create');
    }

    public function rules()
    {
        return [
            'user_id'     => [
                'required',
                'integer',
            ],
            'name'        => [
                'string',
                'required',
            ],
            'token'       => [
                'string',
                'required',
            ],
            'last_digits' => [
                'string',
                'required',
            ],
            'is_default'  => [
                'required',
            ],
        ];
    }
}
