<?php

namespace App\Http\Requests;

use App\Models\User;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('user_edit');
    }

    public function rules()
    {
        return [
            'name'               => [
                'string',
                'required',
            ],
            'last_name'          => [
                'string',
                'required',
            ],
            'email'              => [
                'required',
                'unique:users,email,' . request()->route('user')->id,
            ],
            'roles.*'            => [
                'integer',
            ],
            'roles'              => [
                'required',
                'array',
            ],
            'gender'             => [
                'required',
            ],
            'address_1'          => [
                'string',
                'required',
            ],
            'address_2'          => [
                'string',
                'nullable',
            ],
            'county'             => [
                'string',
                'nullable',
            ],
            'zip_code'           => [
                'string',
                'nullable',
            ],
            'city'               => [
                'string',
                'required',
            ],
            'country'            => [
                'string',
                'required',
            ],
            'phone_number'       => [
                'string',
                'nullable',
            ],
            'phone_verified_at'  => [
                'date_format:' . config('panel.time_format'),
                'nullable',
            ],
            'occupation'         => [
                'string',
                'nullable',
            ],
            'last_activity'      => [
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
                'nullable',
            ],
            'is_subscribed'      => [
                'required',
            ],
            'balance'            => [
                'required',
            ],
            'email_options'      => [
                'required',
            ],
            'banned_at'          => [
                'date_format:' . config('panel.time_format'),
                'nullable',
            ],
            'suspended'          => [
                'required',
            ],
            'positive_passenger' => [
                'required',
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'negative_passenger' => [
                'required',
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'positive_driver'    => [
                'required',
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'negative_driver'    => [
                'required',
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'uuid'               => [
                'string',
                'required',
                'unique:users,uuid,' . request()->route('user')->id,
            ],
        ];
    }
}
