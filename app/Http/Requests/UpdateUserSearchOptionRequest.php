<?php

namespace App\Http\Requests;

use App\Models\UserSearchOption;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateUserSearchOptionRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('user_search_option_edit');
    }

    public function rules()
    {
        return [
            'bike_rack'                => [
                'required',
            ],
            'allowed_smoking'          => [
                'required',
            ],
            'allowed_drinks'           => [
                'required',
            ],
            'allowed_food'             => [
                'required',
            ],
            'allowed_kids'             => [
                'required',
            ],
            'allowed_pets'             => [
                'required',
            ],
            'allowed_3_backseat'       => [
                'required',
            ],
            'allowed_same_gender_ride' => [
                'required',
            ],
            'luggage_space'            => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'allowed_music'            => [
                'required',
            ],
            'allowed_conversation'     => [
                'required',
            ],
            'engine_types'             => [
                'string',
                'nullable',
            ],
            'partial_ride'             => [
                'required',
            ],
            'radius_from'              => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'radius_to'                => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
