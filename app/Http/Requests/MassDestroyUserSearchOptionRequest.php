<?php

namespace App\Http\Requests;

use App\Models\UserSearchOption;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyUserSearchOptionRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('user_search_option_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:user_search_options,id',
        ];
    }
}
