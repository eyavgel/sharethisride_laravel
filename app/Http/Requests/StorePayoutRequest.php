<?php

namespace App\Http\Requests;

use App\Models\Payout;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StorePayoutRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('payout_create');
    }

    public function rules()
    {
        return [
            'user_id'         => [
                'required',
                'integer',
            ],
            'bank_account_id' => [
                'required',
                'integer',
            ],
            'amount'          => [
                'required',
            ],
            'status'          => [
                'required',
            ],
            'exported_at'     => [
                'date_format:' . config('panel.time_format'),
                'nullable',
            ],
        ];
    }
}
