<?php

namespace App\Http\Requests;

use App\Models\Alert;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateAlertRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('alert_edit');
    }

    public function rules()
    {
        return [
            'from_point'               => [
                'string',
                'required',
            ],
            'to_point'                 => [
                'string',
                'required',
            ],
            'leaving_on'               => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'leaving_at'               => [
                'date_format:' . config('panel.time_format'),
                'nullable',
            ],
            'seats'                    => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'bike_rack'                => [
                'required',
            ],
            'engine_types'             => [
                'string',
                'nullable',
            ],
            'allowed_smoking'          => [
                'required',
            ],
            'allowed_drinks'           => [
                'required',
            ],
            'allowed_food'             => [
                'required',
            ],
            'allowed_kids'             => [
                'required',
            ],
            'allowed_pets'             => [
                'required',
            ],
            'allowed_3_backseat'       => [
                'required',
            ],
            'allowed_same_gender_ride' => [
                'required',
            ],
            'luggage_space'            => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'allowed_music'            => [
                'required',
            ],
            'allowed_conversation'     => [
                'required',
            ],
            'partial_ride'             => [
                'required',
            ],
            'radius_from'              => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'radius_to'                => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
