<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAlertRequest;
use App\Http\Requests\UpdateAlertRequest;
use App\Http\Resources\Admin\AlertResource;
use App\Models\Alert;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AlertApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('alert_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AlertResource(Alert::with(['user'])->get());
    }

    public function store(StoreAlertRequest $request)
    {
        $alert = Alert::create($request->all());

        return (new AlertResource($alert))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Alert $alert)
    {
        abort_if(Gate::denies('alert_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new AlertResource($alert->load(['user']));
    }

    public function update(UpdateAlertRequest $request, Alert $alert)
    {
        $alert->update($request->all());

        return (new AlertResource($alert))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Alert $alert)
    {
        abort_if(Gate::denies('alert_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $alert->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
