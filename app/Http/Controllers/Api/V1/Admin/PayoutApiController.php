<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePayoutRequest;
use App\Http\Requests\UpdatePayoutRequest;
use App\Http\Resources\Admin\PayoutResource;
use App\Models\Payout;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PayoutApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('payout_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PayoutResource(Payout::with(['user', 'bank_account'])->get());
    }

    public function store(StorePayoutRequest $request)
    {
        $payout = Payout::create($request->all());

        return (new PayoutResource($payout))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Payout $payout)
    {
        abort_if(Gate::denies('payout_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PayoutResource($payout->load(['user', 'bank_account']));
    }

    public function update(UpdatePayoutRequest $request, Payout $payout)
    {
        $payout->update($request->all());

        return (new PayoutResource($payout))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Payout $payout)
    {
        abort_if(Gate::denies('payout_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $payout->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
