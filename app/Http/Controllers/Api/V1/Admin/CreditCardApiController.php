<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCreditCardRequest;
use App\Http\Requests\UpdateCreditCardRequest;
use App\Http\Resources\Admin\CreditCardResource;
use App\Models\CreditCard;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CreditCardApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('credit_card_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CreditCardResource(CreditCard::with(['user'])->get());
    }

    public function store(StoreCreditCardRequest $request)
    {
        $creditCard = CreditCard::create($request->all());

        return (new CreditCardResource($creditCard))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(CreditCard $creditCard)
    {
        abort_if(Gate::denies('credit_card_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CreditCardResource($creditCard->load(['user']));
    }

    public function update(UpdateCreditCardRequest $request, CreditCard $creditCard)
    {
        $creditCard->update($request->all());

        return (new CreditCardResource($creditCard))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(CreditCard $creditCard)
    {
        abort_if(Gate::denies('credit_card_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $creditCard->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
