<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserSearchOptionRequest;
use App\Http\Requests\UpdateUserSearchOptionRequest;
use App\Http\Resources\Admin\UserSearchOptionResource;
use App\Models\UserSearchOption;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserSearchOptionApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_search_option_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new UserSearchOptionResource(UserSearchOption::with(['user'])->get());
    }

    public function store(StoreUserSearchOptionRequest $request)
    {
        $userSearchOption = UserSearchOption::create($request->all());

        return (new UserSearchOptionResource($userSearchOption))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(UserSearchOption $userSearchOption)
    {
        abort_if(Gate::denies('user_search_option_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new UserSearchOptionResource($userSearchOption->load(['user']));
    }

    public function update(UpdateUserSearchOptionRequest $request, UserSearchOption $userSearchOption)
    {
        $userSearchOption->update($request->all());

        return (new UserSearchOptionResource($userSearchOption))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(UserSearchOption $userSearchOption)
    {
        abort_if(Gate::denies('user_search_option_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $userSearchOption->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
