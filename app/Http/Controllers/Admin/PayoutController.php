<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyPayoutRequest;
use App\Http\Requests\StorePayoutRequest;
use App\Http\Requests\UpdatePayoutRequest;
use App\Models\BankAccount;
use App\Models\Payout;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class PayoutController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('payout_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Payout::with(['user', 'bank_account'])->select(sprintf('%s.*', (new Payout)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'payout_show';
                $editGate      = 'payout_edit';
                $deleteGate    = 'payout_delete';
                $crudRoutePart = 'payouts';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('user_email', function ($row) {
                return $row->user ? $row->user->email : '';
            });

            $table->addColumn('bank_account_number', function ($row) {
                return $row->bank_account ? $row->bank_account->number : '';
            });

            $table->editColumn('amount', function ($row) {
                return $row->amount ? $row->amount : "";
            });
            $table->editColumn('status', function ($row) {
                return $row->status ? Payout::STATUS_RADIO[$row->status] : '';
            });
            $table->editColumn('exported_at', function ($row) {
                return $row->exported_at ? $row->exported_at : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'user', 'bank_account']);

            return $table->make(true);
        }

        return view('admin.payouts.index');
    }

    public function create()
    {
        abort_if(Gate::denies('payout_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $bank_accounts = BankAccount::all()->pluck('number', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.payouts.create', compact('users', 'bank_accounts'));
    }

    public function store(StorePayoutRequest $request)
    {
        $payout = Payout::create($request->all());

        return redirect()->route('admin.payouts.index');
    }

    public function edit(Payout $payout)
    {
        abort_if(Gate::denies('payout_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $bank_accounts = BankAccount::all()->pluck('number', 'id')->prepend(trans('global.pleaseSelect'), '');

        $payout->load('user', 'bank_account');

        return view('admin.payouts.edit', compact('users', 'bank_accounts', 'payout'));
    }

    public function update(UpdatePayoutRequest $request, Payout $payout)
    {
        $payout->update($request->all());

        return redirect()->route('admin.payouts.index');
    }

    public function show(Payout $payout)
    {
        abort_if(Gate::denies('payout_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $payout->load('user', 'bank_account');

        return view('admin.payouts.show', compact('payout'));
    }

    public function destroy(Payout $payout)
    {
        abort_if(Gate::denies('payout_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $payout->delete();

        return back();
    }

    public function massDestroy(MassDestroyPayoutRequest $request)
    {
        Payout::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
