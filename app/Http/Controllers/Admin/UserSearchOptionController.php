<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyUserSearchOptionRequest;
use App\Http\Requests\StoreUserSearchOptionRequest;
use App\Http\Requests\UpdateUserSearchOptionRequest;
use App\Models\User;
use App\Models\UserSearchOption;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class UserSearchOptionController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('user_search_option_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = UserSearchOption::with(['user'])->select(sprintf('%s.*', (new UserSearchOption)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'user_search_option_show';
                $editGate      = 'user_search_option_edit';
                $deleteGate    = 'user_search_option_delete';
                $crudRoutePart = 'user-search-options';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('user_email', function ($row) {
                return $row->user ? $row->user->email : '';
            });

            $table->editColumn('user.email', function ($row) {
                return $row->user ? (is_string($row->user) ? $row->user : $row->user->email) : '';
            });
            $table->editColumn('bike_rack', function ($row) {
                return $row->bike_rack ? UserSearchOption::BIKE_RACK_RADIO[$row->bike_rack] : '';
            });
            $table->editColumn('allowed_smoking', function ($row) {
                return $row->allowed_smoking ? UserSearchOption::ALLOWED_SMOKING_RADIO[$row->allowed_smoking] : '';
            });
            $table->editColumn('allowed_drinks', function ($row) {
                return $row->allowed_drinks ? UserSearchOption::ALLOWED_DRINKS_RADIO[$row->allowed_drinks] : '';
            });
            $table->editColumn('allowed_food', function ($row) {
                return $row->allowed_food ? UserSearchOption::ALLOWED_FOOD_RADIO[$row->allowed_food] : '';
            });
            $table->editColumn('allowed_kids', function ($row) {
                return $row->allowed_kids ? UserSearchOption::ALLOWED_KIDS_RADIO[$row->allowed_kids] : '';
            });
            $table->editColumn('allowed_pets', function ($row) {
                return $row->allowed_pets ? UserSearchOption::ALLOWED_PETS_RADIO[$row->allowed_pets] : '';
            });
            $table->editColumn('allowed_3_backseat', function ($row) {
                return $row->allowed_3_backseat ? UserSearchOption::ALLOWED_3_BACKSEAT_RADIO[$row->allowed_3_backseat] : '';
            });
            $table->editColumn('allowed_same_gender_ride', function ($row) {
                return $row->allowed_same_gender_ride ? UserSearchOption::ALLOWED_SAME_GENDER_RIDE_RADIO[$row->allowed_same_gender_ride] : '';
            });
            $table->editColumn('luggage_space', function ($row) {
                return $row->luggage_space ? $row->luggage_space : "";
            });
            $table->editColumn('allowed_music', function ($row) {
                return $row->allowed_music ? UserSearchOption::ALLOWED_MUSIC_RADIO[$row->allowed_music] : '';
            });
            $table->editColumn('allowed_conversation', function ($row) {
                return $row->allowed_conversation ? UserSearchOption::ALLOWED_CONVERSATION_RADIO[$row->allowed_conversation] : '';
            });
            $table->editColumn('engine_types', function ($row) {
                return $row->engine_types ? $row->engine_types : "";
            });
            $table->editColumn('partial_ride', function ($row) {
                return $row->partial_ride ? UserSearchOption::PARTIAL_RIDE_RADIO[$row->partial_ride] : '';
            });
            $table->editColumn('radius_from', function ($row) {
                return $row->radius_from ? $row->radius_from : "";
            });
            $table->editColumn('radius_to', function ($row) {
                return $row->radius_to ? $row->radius_to : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'user']);

            return $table->make(true);
        }

        return view('admin.userSearchOptions.index');
    }

    public function create()
    {
        abort_if(Gate::denies('user_search_option_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.userSearchOptions.create', compact('users'));
    }

    public function store(StoreUserSearchOptionRequest $request)
    {
        $userSearchOption = UserSearchOption::create($request->all());

        return redirect()->route('admin.user-search-options.index');
    }

    public function edit(UserSearchOption $userSearchOption)
    {
        abort_if(Gate::denies('user_search_option_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $userSearchOption->load('user');

        return view('admin.userSearchOptions.edit', compact('users', 'userSearchOption'));
    }

    public function update(UpdateUserSearchOptionRequest $request, UserSearchOption $userSearchOption)
    {
        $userSearchOption->update($request->all());

        return redirect()->route('admin.user-search-options.index');
    }

    public function show(UserSearchOption $userSearchOption)
    {
        abort_if(Gate::denies('user_search_option_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $userSearchOption->load('user');

        return view('admin.userSearchOptions.show', compact('userSearchOption'));
    }

    public function destroy(UserSearchOption $userSearchOption)
    {
        abort_if(Gate::denies('user_search_option_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $userSearchOption->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserSearchOptionRequest $request)
    {
        UserSearchOption::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
