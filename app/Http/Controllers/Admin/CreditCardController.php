<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCreditCardRequest;
use App\Http\Requests\StoreCreditCardRequest;
use App\Http\Requests\UpdateCreditCardRequest;
use App\Models\CreditCard;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class CreditCardController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('credit_card_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = CreditCard::with(['user'])->select(sprintf('%s.*', (new CreditCard)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'credit_card_show';
                $editGate      = 'credit_card_edit';
                $deleteGate    = 'credit_card_delete';
                $crudRoutePart = 'credit-cards';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('user_email', function ($row) {
                return $row->user ? $row->user->email : '';
            });

            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('token', function ($row) {
                return $row->token ? $row->token : "";
            });
            $table->editColumn('last_digits', function ($row) {
                return $row->last_digits ? $row->last_digits : "";
            });
            $table->editColumn('is_default', function ($row) {
                return $row->is_default ? CreditCard::IS_DEFAULT_RADIO[$row->is_default] : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'user']);

            return $table->make(true);
        }

        return view('admin.creditCards.index');
    }

    public function create()
    {
        abort_if(Gate::denies('credit_card_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.creditCards.create', compact('users'));
    }

    public function store(StoreCreditCardRequest $request)
    {
        $creditCard = CreditCard::create($request->all());

        return redirect()->route('admin.credit-cards.index');
    }

    public function edit(CreditCard $creditCard)
    {
        abort_if(Gate::denies('credit_card_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $creditCard->load('user');

        return view('admin.creditCards.edit', compact('users', 'creditCard'));
    }

    public function update(UpdateCreditCardRequest $request, CreditCard $creditCard)
    {
        $creditCard->update($request->all());

        return redirect()->route('admin.credit-cards.index');
    }

    public function show(CreditCard $creditCard)
    {
        abort_if(Gate::denies('credit_card_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $creditCard->load('user');

        return view('admin.creditCards.show', compact('creditCard'));
    }

    public function destroy(CreditCard $creditCard)
    {
        abort_if(Gate::denies('credit_card_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $creditCard->delete();

        return back();
    }

    public function massDestroy(MassDestroyCreditCardRequest $request)
    {
        CreditCard::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
