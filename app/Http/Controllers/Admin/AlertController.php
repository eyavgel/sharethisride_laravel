<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyAlertRequest;
use App\Http\Requests\StoreAlertRequest;
use App\Http\Requests\UpdateAlertRequest;
use App\Models\Alert;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class AlertController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('alert_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Alert::with(['user'])->select(sprintf('%s.*', (new Alert)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'alert_show';
                $editGate      = 'alert_edit';
                $deleteGate    = 'alert_delete';
                $crudRoutePart = 'alerts';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('user_email', function ($row) {
                return $row->user ? $row->user->email : '';
            });

            $table->editColumn('user.email', function ($row) {
                return $row->user ? (is_string($row->user) ? $row->user : $row->user->email) : '';
            });
            $table->editColumn('from_point', function ($row) {
                return $row->from_point ? $row->from_point : "";
            });
            $table->editColumn('to_point', function ($row) {
                return $row->to_point ? $row->to_point : "";
            });

            $table->editColumn('leaving_at', function ($row) {
                return $row->leaving_at ? $row->leaving_at : "";
            });
            $table->editColumn('seats', function ($row) {
                return $row->seats ? $row->seats : "";
            });
            $table->editColumn('price', function ($row) {
                return $row->price ? $row->price : "";
            });
            $table->editColumn('bike_rack', function ($row) {
                return $row->bike_rack ? Alert::BIKE_RACK_RADIO[$row->bike_rack] : '';
            });
            $table->editColumn('engine_types', function ($row) {
                return $row->engine_types ? $row->engine_types : "";
            });
            $table->editColumn('allowed_smoking', function ($row) {
                return $row->allowed_smoking ? Alert::ALLOWED_SMOKING_RADIO[$row->allowed_smoking] : '';
            });
            $table->editColumn('allowed_drinks', function ($row) {
                return $row->allowed_drinks ? Alert::ALLOWED_DRINKS_RADIO[$row->allowed_drinks] : '';
            });
            $table->editColumn('allowed_food', function ($row) {
                return $row->allowed_food ? Alert::ALLOWED_FOOD_RADIO[$row->allowed_food] : '';
            });
            $table->editColumn('allowed_kids', function ($row) {
                return $row->allowed_kids ? Alert::ALLOWED_KIDS_RADIO[$row->allowed_kids] : '';
            });
            $table->editColumn('allowed_pets', function ($row) {
                return $row->allowed_pets ? Alert::ALLOWED_PETS_RADIO[$row->allowed_pets] : '';
            });
            $table->editColumn('allowed_3_backseat', function ($row) {
                return $row->allowed_3_backseat ? Alert::ALLOWED_3_BACKSEAT_RADIO[$row->allowed_3_backseat] : '';
            });
            $table->editColumn('allowed_same_gender_ride', function ($row) {
                return $row->allowed_same_gender_ride ? Alert::ALLOWED_SAME_GENDER_RIDE_RADIO[$row->allowed_same_gender_ride] : '';
            });
            $table->editColumn('luggage_space', function ($row) {
                return $row->luggage_space ? $row->luggage_space : "";
            });
            $table->editColumn('allowed_music', function ($row) {
                return $row->allowed_music ? Alert::ALLOWED_MUSIC_RADIO[$row->allowed_music] : '';
            });
            $table->editColumn('allowed_conversation', function ($row) {
                return $row->allowed_conversation ? Alert::ALLOWED_CONVERSATION_RADIO[$row->allowed_conversation] : '';
            });
            $table->editColumn('partial_ride', function ($row) {
                return $row->partial_ride ? Alert::PARTIAL_RIDE_RADIO[$row->partial_ride] : '';
            });
            $table->editColumn('radius_from', function ($row) {
                return $row->radius_from ? $row->radius_from : "";
            });
            $table->editColumn('radius_to', function ($row) {
                return $row->radius_to ? $row->radius_to : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'user']);

            return $table->make(true);
        }

        return view('admin.alerts.index');
    }

    public function create()
    {
        abort_if(Gate::denies('alert_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.alerts.create', compact('users'));
    }

    public function store(StoreAlertRequest $request)
    {
        $alert = Alert::create($request->all());

        return redirect()->route('admin.alerts.index');
    }

    public function edit(Alert $alert)
    {
        abort_if(Gate::denies('alert_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $alert->load('user');

        return view('admin.alerts.edit', compact('users', 'alert'));
    }

    public function update(UpdateAlertRequest $request, Alert $alert)
    {
        $alert->update($request->all());

        return redirect()->route('admin.alerts.index');
    }

    public function show(Alert $alert)
    {
        abort_if(Gate::denies('alert_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $alert->load('user');

        return view('admin.alerts.show', compact('alert'));
    }

    public function destroy(Alert $alert)
    {
        abort_if(Gate::denies('alert_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $alert->delete();

        return back();
    }

    public function massDestroy(MassDestroyAlertRequest $request)
    {
        Alert::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
