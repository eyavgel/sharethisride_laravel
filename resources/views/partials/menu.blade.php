<div id="sidebar" class="c-sidebar c-sidebar-fixed c-sidebar-lg-show">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="#">
            {{ trans('panel.site_title') }}
        </a>
    </div>

    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>
        @can('user_management_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/permissions*") ? "c-show" : "" }} {{ request()->is("admin/roles*") ? "c-show" : "" }} {{ request()->is("admin/users*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.userManagement.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    @can('permission_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-unlock-alt c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.permission.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('role_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-briefcase c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.role.title') }}
                            </a>
                        </li>
                    @endcan
                    @can('user_access')
                        <li class="c-sidebar-nav-item">
                            <a href="{{ route("admin.users.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/users") || request()->is("admin/users/*") ? "c-active" : "" }}">
                                <i class="fa-fw fas fa-user c-sidebar-nav-icon">

                                </i>
                                {{ trans('cruds.user.title') }}
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('user_search_option_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.user-search-options.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/user-search-options") || request()->is("admin/user-search-options/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-search c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.userSearchOption.title') }}
                </a>
            </li>
        @endcan
        @can('bank_account_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.bank-accounts.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/bank-accounts") || request()->is("admin/bank-accounts/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-money-check-alt c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.bankAccount.title') }}
                </a>
            </li>
        @endcan
        @can('payout_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.payouts.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/payouts") || request()->is("admin/payouts/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-piggy-bank c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.payout.title') }}
                </a>
            </li>
        @endcan
        @can('credit_card_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.credit-cards.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/credit-cards") || request()->is("admin/credit-cards/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-credit-card c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.creditCard.title') }}
                </a>
            </li>
        @endcan
        @can('alert_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.alerts.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/alerts") || request()->is("admin/alerts/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-bell c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.alert.title') }}
                </a>
            </li>
        @endcan
        @can('car_access')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.cars.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/cars") || request()->is("admin/cars/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-car c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.car.title') }}
                </a>
            </li>
        @endcan
        @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
            @can('profile_password_edit')
                <li class="c-sidebar-nav-item">
                    <a class="c-sidebar-nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'c-active' : '' }}" href="{{ route('profile.password.edit') }}">
                        <i class="fa-fw fas fa-key c-sidebar-nav-icon">
                        </i>
                        {{ trans('global.change_password') }}
                    </a>
                </li>
            @endcan
        @endif
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

                </i>
                {{ trans('global.logout') }}
            </a>
        </li>
    </ul>

</div>