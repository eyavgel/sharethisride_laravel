@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.creditCard.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.credit-cards.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.creditCard.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.creditCard.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.creditCard.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.creditCard.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="token">{{ trans('cruds.creditCard.fields.token') }}</label>
                <input class="form-control {{ $errors->has('token') ? 'is-invalid' : '' }}" type="text" name="token" id="token" value="{{ old('token', '') }}" required>
                @if($errors->has('token'))
                    <div class="invalid-feedback">
                        {{ $errors->first('token') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.creditCard.fields.token_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="last_digits">{{ trans('cruds.creditCard.fields.last_digits') }}</label>
                <input class="form-control {{ $errors->has('last_digits') ? 'is-invalid' : '' }}" type="text" name="last_digits" id="last_digits" value="{{ old('last_digits', '') }}" required>
                @if($errors->has('last_digits'))
                    <div class="invalid-feedback">
                        {{ $errors->first('last_digits') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.creditCard.fields.last_digits_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.creditCard.fields.is_default') }}</label>
                @foreach(App\Models\CreditCard::IS_DEFAULT_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('is_default') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="is_default_{{ $key }}" name="is_default" value="{{ $key }}" {{ old('is_default', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="is_default_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('is_default'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_default') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.creditCard.fields.is_default_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection