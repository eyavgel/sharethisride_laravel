@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.creditCard.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.credit-cards.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.creditCard.fields.id') }}
                        </th>
                        <td>
                            {{ $creditCard->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.creditCard.fields.user') }}
                        </th>
                        <td>
                            {{ $creditCard->user->email ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.creditCard.fields.name') }}
                        </th>
                        <td>
                            {{ $creditCard->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.creditCard.fields.token') }}
                        </th>
                        <td>
                            {{ $creditCard->token }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.creditCard.fields.last_digits') }}
                        </th>
                        <td>
                            {{ $creditCard->last_digits }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.creditCard.fields.is_default') }}
                        </th>
                        <td>
                            {{ App\Models\CreditCard::IS_DEFAULT_RADIO[$creditCard->is_default] ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.credit-cards.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection