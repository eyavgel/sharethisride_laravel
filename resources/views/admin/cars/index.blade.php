@extends('layouts.admin')
@section('content')
@can('car_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.cars.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.car.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.car.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Car">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.car.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.car.fields.user') }}
                    </th>
                    <th>
                        {{ trans('cruds.car.fields.manufacturer') }}
                    </th>
                    <th>
                        {{ trans('cruds.car.fields.model') }}
                    </th>
                    <th>
                        {{ trans('cruds.car.fields.seats_number') }}
                    </th>
                    <th>
                        {{ trans('cruds.car.fields.production_year') }}
                    </th>
                    <th>
                        {{ trans('cruds.car.fields.engine') }}
                    </th>
                    <th>
                        {{ trans('cruds.car.fields.is_default') }}
                    </th>
                    <th>
                        {{ trans('cruds.car.fields.is_visible') }}
                    </th>
                    <th>
                        {{ trans('cruds.car.fields.position') }}
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('car_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.cars.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.cars.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'user_email', name: 'user.email' },
{ data: 'manufacturer', name: 'manufacturer' },
{ data: 'model', name: 'model' },
{ data: 'seats_number', name: 'seats_number' },
{ data: 'production_year', name: 'production_year' },
{ data: 'engine', name: 'engine' },
{ data: 'is_default', name: 'is_default' },
{ data: 'is_visible', name: 'is_visible' },
{ data: 'position', name: 'position' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  let table = $('.datatable-Car').DataTable(dtOverrideGlobals);
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
});

</script>
@endsection