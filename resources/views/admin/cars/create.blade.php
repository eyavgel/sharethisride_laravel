@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.car.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.cars.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.car.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.car.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="manufacturer">{{ trans('cruds.car.fields.manufacturer') }}</label>
                <input class="form-control {{ $errors->has('manufacturer') ? 'is-invalid' : '' }}" type="text" name="manufacturer" id="manufacturer" value="{{ old('manufacturer', '') }}" required>
                @if($errors->has('manufacturer'))
                    <div class="invalid-feedback">
                        {{ $errors->first('manufacturer') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.car.fields.manufacturer_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="model">{{ trans('cruds.car.fields.model') }}</label>
                <input class="form-control {{ $errors->has('model') ? 'is-invalid' : '' }}" type="text" name="model" id="model" value="{{ old('model', '') }}" required>
                @if($errors->has('model'))
                    <div class="invalid-feedback">
                        {{ $errors->first('model') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.car.fields.model_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="seats_number">{{ trans('cruds.car.fields.seats_number') }}</label>
                <input class="form-control {{ $errors->has('seats_number') ? 'is-invalid' : '' }}" type="number" name="seats_number" id="seats_number" value="{{ old('seats_number', '') }}" step="1" required>
                @if($errors->has('seats_number'))
                    <div class="invalid-feedback">
                        {{ $errors->first('seats_number') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.car.fields.seats_number_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="production_year">{{ trans('cruds.car.fields.production_year') }}</label>
                <input class="form-control date {{ $errors->has('production_year') ? 'is-invalid' : '' }}" type="text" name="production_year" id="production_year" value="{{ old('production_year') }}" required>
                @if($errors->has('production_year'))
                    <div class="invalid-feedback">
                        {{ $errors->first('production_year') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.car.fields.production_year_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="engine">{{ trans('cruds.car.fields.engine') }}</label>
                <input class="form-control {{ $errors->has('engine') ? 'is-invalid' : '' }}" type="text" name="engine" id="engine" value="{{ old('engine', '') }}" required>
                @if($errors->has('engine'))
                    <div class="invalid-feedback">
                        {{ $errors->first('engine') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.car.fields.engine_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.car.fields.is_default') }}</label>
                @foreach(App\Models\Car::IS_DEFAULT_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('is_default') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="is_default_{{ $key }}" name="is_default" value="{{ $key }}" {{ old('is_default', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="is_default_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('is_default'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_default') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.car.fields.is_default_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.car.fields.is_visible') }}</label>
                @foreach(App\Models\Car::IS_VISIBLE_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('is_visible') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="is_visible_{{ $key }}" name="is_visible" value="{{ $key }}" {{ old('is_visible', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="is_visible_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('is_visible'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_visible') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.car.fields.is_visible_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="position">{{ trans('cruds.car.fields.position') }}</label>
                <input class="form-control {{ $errors->has('position') ? 'is-invalid' : '' }}" type="number" name="position" id="position" value="{{ old('position', '') }}" step="1" required>
                @if($errors->has('position'))
                    <div class="invalid-feedback">
                        {{ $errors->first('position') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.car.fields.position_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection