@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.userSearchOption.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.user-search-options.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="user_id">{{ trans('cruds.userSearchOption.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id">
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.bike_rack') }}</label>
                @foreach(App\Models\UserSearchOption::BIKE_RACK_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('bike_rack') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="bike_rack_{{ $key }}" name="bike_rack" value="{{ $key }}" {{ old('bike_rack', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="bike_rack_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('bike_rack'))
                    <div class="invalid-feedback">
                        {{ $errors->first('bike_rack') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.bike_rack_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.allowed_smoking') }}</label>
                @foreach(App\Models\UserSearchOption::ALLOWED_SMOKING_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('allowed_smoking') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="allowed_smoking_{{ $key }}" name="allowed_smoking" value="{{ $key }}" {{ old('allowed_smoking', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="allowed_smoking_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('allowed_smoking'))
                    <div class="invalid-feedback">
                        {{ $errors->first('allowed_smoking') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.allowed_smoking_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.allowed_drinks') }}</label>
                @foreach(App\Models\UserSearchOption::ALLOWED_DRINKS_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('allowed_drinks') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="allowed_drinks_{{ $key }}" name="allowed_drinks" value="{{ $key }}" {{ old('allowed_drinks', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="allowed_drinks_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('allowed_drinks'))
                    <div class="invalid-feedback">
                        {{ $errors->first('allowed_drinks') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.allowed_drinks_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.allowed_food') }}</label>
                @foreach(App\Models\UserSearchOption::ALLOWED_FOOD_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('allowed_food') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="allowed_food_{{ $key }}" name="allowed_food" value="{{ $key }}" {{ old('allowed_food', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="allowed_food_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('allowed_food'))
                    <div class="invalid-feedback">
                        {{ $errors->first('allowed_food') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.allowed_food_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.allowed_kids') }}</label>
                @foreach(App\Models\UserSearchOption::ALLOWED_KIDS_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('allowed_kids') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="allowed_kids_{{ $key }}" name="allowed_kids" value="{{ $key }}" {{ old('allowed_kids', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="allowed_kids_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('allowed_kids'))
                    <div class="invalid-feedback">
                        {{ $errors->first('allowed_kids') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.allowed_kids_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.allowed_pets') }}</label>
                @foreach(App\Models\UserSearchOption::ALLOWED_PETS_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('allowed_pets') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="allowed_pets_{{ $key }}" name="allowed_pets" value="{{ $key }}" {{ old('allowed_pets', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="allowed_pets_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('allowed_pets'))
                    <div class="invalid-feedback">
                        {{ $errors->first('allowed_pets') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.allowed_pets_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.allowed_3_backseat') }}</label>
                @foreach(App\Models\UserSearchOption::ALLOWED_3_BACKSEAT_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('allowed_3_backseat') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="allowed_3_backseat_{{ $key }}" name="allowed_3_backseat" value="{{ $key }}" {{ old('allowed_3_backseat', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="allowed_3_backseat_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('allowed_3_backseat'))
                    <div class="invalid-feedback">
                        {{ $errors->first('allowed_3_backseat') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.allowed_3_backseat_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.allowed_same_gender_ride') }}</label>
                @foreach(App\Models\UserSearchOption::ALLOWED_SAME_GENDER_RIDE_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('allowed_same_gender_ride') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="allowed_same_gender_ride_{{ $key }}" name="allowed_same_gender_ride" value="{{ $key }}" {{ old('allowed_same_gender_ride', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="allowed_same_gender_ride_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('allowed_same_gender_ride'))
                    <div class="invalid-feedback">
                        {{ $errors->first('allowed_same_gender_ride') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.allowed_same_gender_ride_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="luggage_space">{{ trans('cruds.userSearchOption.fields.luggage_space') }}</label>
                <input class="form-control {{ $errors->has('luggage_space') ? 'is-invalid' : '' }}" type="number" name="luggage_space" id="luggage_space" value="{{ old('luggage_space', '') }}" step="1" required>
                @if($errors->has('luggage_space'))
                    <div class="invalid-feedback">
                        {{ $errors->first('luggage_space') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.luggage_space_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.allowed_music') }}</label>
                @foreach(App\Models\UserSearchOption::ALLOWED_MUSIC_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('allowed_music') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="allowed_music_{{ $key }}" name="allowed_music" value="{{ $key }}" {{ old('allowed_music', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="allowed_music_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('allowed_music'))
                    <div class="invalid-feedback">
                        {{ $errors->first('allowed_music') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.allowed_music_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.allowed_conversation') }}</label>
                @foreach(App\Models\UserSearchOption::ALLOWED_CONVERSATION_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('allowed_conversation') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="allowed_conversation_{{ $key }}" name="allowed_conversation" value="{{ $key }}" {{ old('allowed_conversation', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="allowed_conversation_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('allowed_conversation'))
                    <div class="invalid-feedback">
                        {{ $errors->first('allowed_conversation') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.allowed_conversation_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="engine_types">{{ trans('cruds.userSearchOption.fields.engine_types') }}</label>
                <input class="form-control {{ $errors->has('engine_types') ? 'is-invalid' : '' }}" type="text" name="engine_types" id="engine_types" value="{{ old('engine_types', '') }}">
                @if($errors->has('engine_types'))
                    <div class="invalid-feedback">
                        {{ $errors->first('engine_types') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.engine_types_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.userSearchOption.fields.partial_ride') }}</label>
                @foreach(App\Models\UserSearchOption::PARTIAL_RIDE_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('partial_ride') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="partial_ride_{{ $key }}" name="partial_ride" value="{{ $key }}" {{ old('partial_ride', '') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="partial_ride_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('partial_ride'))
                    <div class="invalid-feedback">
                        {{ $errors->first('partial_ride') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.partial_ride_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="radius_from">{{ trans('cruds.userSearchOption.fields.radius_from') }}</label>
                <input class="form-control {{ $errors->has('radius_from') ? 'is-invalid' : '' }}" type="number" name="radius_from" id="radius_from" value="{{ old('radius_from', '') }}" step="1" required>
                @if($errors->has('radius_from'))
                    <div class="invalid-feedback">
                        {{ $errors->first('radius_from') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.radius_from_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="radius_to">{{ trans('cruds.userSearchOption.fields.radius_to') }}</label>
                <input class="form-control {{ $errors->has('radius_to') ? 'is-invalid' : '' }}" type="number" name="radius_to" id="radius_to" value="{{ old('radius_to', '') }}" step="1" required>
                @if($errors->has('radius_to'))
                    <div class="invalid-feedback">
                        {{ $errors->first('radius_to') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userSearchOption.fields.radius_to_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection