@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.userSearchOption.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.user-search-options.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.id') }}
                        </th>
                        <td>
                            {{ $userSearchOption->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.user') }}
                        </th>
                        <td>
                            {{ $userSearchOption->user->email ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.bike_rack') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::BIKE_RACK_RADIO[$userSearchOption->bike_rack] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.allowed_smoking') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::ALLOWED_SMOKING_RADIO[$userSearchOption->allowed_smoking] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.allowed_drinks') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::ALLOWED_DRINKS_RADIO[$userSearchOption->allowed_drinks] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.allowed_food') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::ALLOWED_FOOD_RADIO[$userSearchOption->allowed_food] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.allowed_kids') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::ALLOWED_KIDS_RADIO[$userSearchOption->allowed_kids] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.allowed_pets') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::ALLOWED_PETS_RADIO[$userSearchOption->allowed_pets] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.allowed_3_backseat') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::ALLOWED_3_BACKSEAT_RADIO[$userSearchOption->allowed_3_backseat] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.allowed_same_gender_ride') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::ALLOWED_SAME_GENDER_RIDE_RADIO[$userSearchOption->allowed_same_gender_ride] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.luggage_space') }}
                        </th>
                        <td>
                            {{ $userSearchOption->luggage_space }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.allowed_music') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::ALLOWED_MUSIC_RADIO[$userSearchOption->allowed_music] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.allowed_conversation') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::ALLOWED_CONVERSATION_RADIO[$userSearchOption->allowed_conversation] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.engine_types') }}
                        </th>
                        <td>
                            {{ $userSearchOption->engine_types }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.partial_ride') }}
                        </th>
                        <td>
                            {{ App\Models\UserSearchOption::PARTIAL_RIDE_RADIO[$userSearchOption->partial_ride] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.radius_from') }}
                        </th>
                        <td>
                            {{ $userSearchOption->radius_from }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.userSearchOption.fields.radius_to') }}
                        </th>
                        <td>
                            {{ $userSearchOption->radius_to }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.user-search-options.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection