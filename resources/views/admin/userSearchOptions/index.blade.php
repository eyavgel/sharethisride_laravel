@extends('layouts.admin')
@section('content')
@can('user_search_option_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.user-search-options.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.userSearchOption.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.userSearchOption.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-UserSearchOption">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.user') }}
                    </th>
                    <th>
                        {{ trans('cruds.user.fields.email') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.bike_rack') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.allowed_smoking') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.allowed_drinks') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.allowed_food') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.allowed_kids') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.allowed_pets') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.allowed_3_backseat') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.allowed_same_gender_ride') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.luggage_space') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.allowed_music') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.allowed_conversation') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.engine_types') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.partial_ride') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.radius_from') }}
                    </th>
                    <th>
                        {{ trans('cruds.userSearchOption.fields.radius_to') }}
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('user_search_option_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.user-search-options.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.user-search-options.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'user_email', name: 'user.email' },
{ data: 'user.email', name: 'user.email' },
{ data: 'bike_rack', name: 'bike_rack' },
{ data: 'allowed_smoking', name: 'allowed_smoking' },
{ data: 'allowed_drinks', name: 'allowed_drinks' },
{ data: 'allowed_food', name: 'allowed_food' },
{ data: 'allowed_kids', name: 'allowed_kids' },
{ data: 'allowed_pets', name: 'allowed_pets' },
{ data: 'allowed_3_backseat', name: 'allowed_3_backseat' },
{ data: 'allowed_same_gender_ride', name: 'allowed_same_gender_ride' },
{ data: 'luggage_space', name: 'luggage_space' },
{ data: 'allowed_music', name: 'allowed_music' },
{ data: 'allowed_conversation', name: 'allowed_conversation' },
{ data: 'engine_types', name: 'engine_types' },
{ data: 'partial_ride', name: 'partial_ride' },
{ data: 'radius_from', name: 'radius_from' },
{ data: 'radius_to', name: 'radius_to' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  let table = $('.datatable-UserSearchOption').DataTable(dtOverrideGlobals);
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
});

</script>
@endsection