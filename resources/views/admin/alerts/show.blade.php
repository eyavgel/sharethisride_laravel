@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.alert.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.alerts.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.id') }}
                        </th>
                        <td>
                            {{ $alert->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.user') }}
                        </th>
                        <td>
                            {{ $alert->user->email ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.from_point') }}
                        </th>
                        <td>
                            {{ $alert->from_point }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.to_point') }}
                        </th>
                        <td>
                            {{ $alert->to_point }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.leaving_on') }}
                        </th>
                        <td>
                            {{ $alert->leaving_on }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.leaving_at') }}
                        </th>
                        <td>
                            {{ $alert->leaving_at }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.seats') }}
                        </th>
                        <td>
                            {{ $alert->seats }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.price') }}
                        </th>
                        <td>
                            {{ $alert->price }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.bike_rack') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::BIKE_RACK_RADIO[$alert->bike_rack] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.engine_types') }}
                        </th>
                        <td>
                            {{ $alert->engine_types }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.allowed_smoking') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::ALLOWED_SMOKING_RADIO[$alert->allowed_smoking] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.allowed_drinks') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::ALLOWED_DRINKS_RADIO[$alert->allowed_drinks] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.allowed_food') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::ALLOWED_FOOD_RADIO[$alert->allowed_food] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.allowed_kids') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::ALLOWED_KIDS_RADIO[$alert->allowed_kids] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.allowed_pets') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::ALLOWED_PETS_RADIO[$alert->allowed_pets] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.allowed_3_backseat') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::ALLOWED_3_BACKSEAT_RADIO[$alert->allowed_3_backseat] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.allowed_same_gender_ride') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::ALLOWED_SAME_GENDER_RIDE_RADIO[$alert->allowed_same_gender_ride] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.luggage_space') }}
                        </th>
                        <td>
                            {{ $alert->luggage_space }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.allowed_music') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::ALLOWED_MUSIC_RADIO[$alert->allowed_music] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.allowed_conversation') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::ALLOWED_CONVERSATION_RADIO[$alert->allowed_conversation] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.partial_ride') }}
                        </th>
                        <td>
                            {{ App\Models\Alert::PARTIAL_RIDE_RADIO[$alert->partial_ride] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.radius_from') }}
                        </th>
                        <td>
                            {{ $alert->radius_from }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.alert.fields.radius_to') }}
                        </th>
                        <td>
                            {{ $alert->radius_to }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.alerts.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection