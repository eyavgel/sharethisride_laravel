@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.user.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.users.update", [$user->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.user.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $user->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="last_name">{{ trans('cruds.user.fields.last_name') }}</label>
                <input class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}" type="text" name="last_name" id="last_name" value="{{ old('last_name', $user->last_name) }}" required>
                @if($errors->has('last_name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('last_name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.last_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="email">{{ trans('cruds.user.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email', $user->email) }}" required>
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="password">{{ trans('cruds.user.fields.password') }}</label>
                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" id="password">
                @if($errors->has('password'))
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.password_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="roles">{{ trans('cruds.user.fields.roles') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('roles') ? 'is-invalid' : '' }}" name="roles[]" id="roles" multiple required>
                    @foreach($roles as $id => $roles)
                        <option value="{{ $id }}" {{ (in_array($id, old('roles', [])) || $user->roles->contains($id)) ? 'selected' : '' }}>{{ $roles }}</option>
                    @endforeach
                </select>
                @if($errors->has('roles'))
                    <div class="invalid-feedback">
                        {{ $errors->first('roles') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.roles_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.user.fields.gender') }}</label>
                @foreach(App\Models\User::GENDER_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('gender') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="gender_{{ $key }}" name="gender" value="{{ $key }}" {{ old('gender', $user->gender) === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="gender_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('gender'))
                    <div class="invalid-feedback">
                        {{ $errors->first('gender') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.gender_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="address_1">{{ trans('cruds.user.fields.address_1') }}</label>
                <input class="form-control {{ $errors->has('address_1') ? 'is-invalid' : '' }}" type="text" name="address_1" id="address_1" value="{{ old('address_1', $user->address_1) }}" required>
                @if($errors->has('address_1'))
                    <div class="invalid-feedback">
                        {{ $errors->first('address_1') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.address_1_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="address_2">{{ trans('cruds.user.fields.address_2') }}</label>
                <input class="form-control {{ $errors->has('address_2') ? 'is-invalid' : '' }}" type="text" name="address_2" id="address_2" value="{{ old('address_2', $user->address_2) }}">
                @if($errors->has('address_2'))
                    <div class="invalid-feedback">
                        {{ $errors->first('address_2') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.address_2_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="county">{{ trans('cruds.user.fields.county') }}</label>
                <input class="form-control {{ $errors->has('county') ? 'is-invalid' : '' }}" type="text" name="county" id="county" value="{{ old('county', $user->county) }}">
                @if($errors->has('county'))
                    <div class="invalid-feedback">
                        {{ $errors->first('county') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.county_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="zip_code">{{ trans('cruds.user.fields.zip_code') }}</label>
                <input class="form-control {{ $errors->has('zip_code') ? 'is-invalid' : '' }}" type="text" name="zip_code" id="zip_code" value="{{ old('zip_code', $user->zip_code) }}">
                @if($errors->has('zip_code'))
                    <div class="invalid-feedback">
                        {{ $errors->first('zip_code') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.zip_code_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="city">{{ trans('cruds.user.fields.city') }}</label>
                <input class="form-control {{ $errors->has('city') ? 'is-invalid' : '' }}" type="text" name="city" id="city" value="{{ old('city', $user->city) }}" required>
                @if($errors->has('city'))
                    <div class="invalid-feedback">
                        {{ $errors->first('city') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.city_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="country">{{ trans('cruds.user.fields.country') }}</label>
                <input class="form-control {{ $errors->has('country') ? 'is-invalid' : '' }}" type="text" name="country" id="country" value="{{ old('country', $user->country) }}" required>
                @if($errors->has('country'))
                    <div class="invalid-feedback">
                        {{ $errors->first('country') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.country_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="phone_number">{{ trans('cruds.user.fields.phone_number') }}</label>
                <input class="form-control {{ $errors->has('phone_number') ? 'is-invalid' : '' }}" type="text" name="phone_number" id="phone_number" value="{{ old('phone_number', $user->phone_number) }}">
                @if($errors->has('phone_number'))
                    <div class="invalid-feedback">
                        {{ $errors->first('phone_number') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.phone_number_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="phone_verified_at">{{ trans('cruds.user.fields.phone_verified_at') }}</label>
                <input class="form-control timepicker {{ $errors->has('phone_verified_at') ? 'is-invalid' : '' }}" type="text" name="phone_verified_at" id="phone_verified_at" value="{{ old('phone_verified_at', $user->phone_verified_at) }}">
                @if($errors->has('phone_verified_at'))
                    <div class="invalid-feedback">
                        {{ $errors->first('phone_verified_at') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.phone_verified_at_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="about">{{ trans('cruds.user.fields.about') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('about') ? 'is-invalid' : '' }}" name="about" id="about">{!! old('about', $user->about) !!}</textarea>
                @if($errors->has('about'))
                    <div class="invalid-feedback">
                        {{ $errors->first('about') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.about_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="occupation">{{ trans('cruds.user.fields.occupation') }}</label>
                <input class="form-control {{ $errors->has('occupation') ? 'is-invalid' : '' }}" type="text" name="occupation" id="occupation" value="{{ old('occupation', $user->occupation) }}">
                @if($errors->has('occupation'))
                    <div class="invalid-feedback">
                        {{ $errors->first('occupation') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.occupation_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="last_activity">{{ trans('cruds.user.fields.last_activity') }}</label>
                <input class="form-control datetime {{ $errors->has('last_activity') ? 'is-invalid' : '' }}" type="text" name="last_activity" id="last_activity" value="{{ old('last_activity', $user->last_activity) }}">
                @if($errors->has('last_activity'))
                    <div class="invalid-feedback">
                        {{ $errors->first('last_activity') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.last_activity_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.user.fields.is_subscribed') }}</label>
                @foreach(App\Models\User::IS_SUBSCRIBED_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('is_subscribed') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="is_subscribed_{{ $key }}" name="is_subscribed" value="{{ $key }}" {{ old('is_subscribed', $user->is_subscribed) === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="is_subscribed_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('is_subscribed'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_subscribed') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.is_subscribed_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="balance">{{ trans('cruds.user.fields.balance') }}</label>
                <input class="form-control {{ $errors->has('balance') ? 'is-invalid' : '' }}" type="number" name="balance" id="balance" value="{{ old('balance', $user->balance) }}" step="0.01" required>
                @if($errors->has('balance'))
                    <div class="invalid-feedback">
                        {{ $errors->first('balance') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.balance_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="email_options">{{ trans('cruds.user.fields.email_options') }}</label>
                <textarea class="form-control {{ $errors->has('email_options') ? 'is-invalid' : '' }}" name="email_options" id="email_options" required>{{ old('email_options', $user->email_options) }}</textarea>
                @if($errors->has('email_options'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email_options') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.email_options_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="banned_at">{{ trans('cruds.user.fields.banned_at') }}</label>
                <input class="form-control timepicker {{ $errors->has('banned_at') ? 'is-invalid' : '' }}" type="text" name="banned_at" id="banned_at" value="{{ old('banned_at', $user->banned_at) }}">
                @if($errors->has('banned_at'))
                    <div class="invalid-feedback">
                        {{ $errors->first('banned_at') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.banned_at_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.user.fields.suspended') }}</label>
                @foreach(App\Models\User::SUSPENDED_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('suspended') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="suspended_{{ $key }}" name="suspended" value="{{ $key }}" {{ old('suspended', $user->suspended) === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="suspended_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('suspended'))
                    <div class="invalid-feedback">
                        {{ $errors->first('suspended') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.suspended_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="positive_passenger">{{ trans('cruds.user.fields.positive_passenger') }}</label>
                <input class="form-control {{ $errors->has('positive_passenger') ? 'is-invalid' : '' }}" type="number" name="positive_passenger" id="positive_passenger" value="{{ old('positive_passenger', $user->positive_passenger) }}" step="1" required>
                @if($errors->has('positive_passenger'))
                    <div class="invalid-feedback">
                        {{ $errors->first('positive_passenger') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.positive_passenger_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="negative_passenger">{{ trans('cruds.user.fields.negative_passenger') }}</label>
                <input class="form-control {{ $errors->has('negative_passenger') ? 'is-invalid' : '' }}" type="number" name="negative_passenger" id="negative_passenger" value="{{ old('negative_passenger', $user->negative_passenger) }}" step="1" required>
                @if($errors->has('negative_passenger'))
                    <div class="invalid-feedback">
                        {{ $errors->first('negative_passenger') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.negative_passenger_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="positive_driver">{{ trans('cruds.user.fields.positive_driver') }}</label>
                <input class="form-control {{ $errors->has('positive_driver') ? 'is-invalid' : '' }}" type="number" name="positive_driver" id="positive_driver" value="{{ old('positive_driver', $user->positive_driver) }}" step="1" required>
                @if($errors->has('positive_driver'))
                    <div class="invalid-feedback">
                        {{ $errors->first('positive_driver') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.positive_driver_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="negative_driver">{{ trans('cruds.user.fields.negative_driver') }}</label>
                <input class="form-control {{ $errors->has('negative_driver') ? 'is-invalid' : '' }}" type="number" name="negative_driver" id="negative_driver" value="{{ old('negative_driver', $user->negative_driver) }}" step="1" required>
                @if($errors->has('negative_driver'))
                    <div class="invalid-feedback">
                        {{ $errors->first('negative_driver') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.negative_driver_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="uuid">{{ trans('cruds.user.fields.uuid') }}</label>
                <input class="form-control {{ $errors->has('uuid') ? 'is-invalid' : '' }}" type="text" name="uuid" id="uuid" value="{{ old('uuid', $user->uuid) }}" required>
                @if($errors->has('uuid'))
                    <div class="invalid-feedback">
                        {{ $errors->first('uuid') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.user.fields.uuid_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/users/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $user->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection