@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.payout.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.payouts.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.payout.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <div class="invalid-feedback">
                        {{ $errors->first('user') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.payout.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="bank_account_id">{{ trans('cruds.payout.fields.bank_account') }}</label>
                <select class="form-control select2 {{ $errors->has('bank_account') ? 'is-invalid' : '' }}" name="bank_account_id" id="bank_account_id" required>
                    @foreach($bank_accounts as $id => $bank_account)
                        <option value="{{ $id }}" {{ old('bank_account_id') == $id ? 'selected' : '' }}>{{ $bank_account }}</option>
                    @endforeach
                </select>
                @if($errors->has('bank_account'))
                    <div class="invalid-feedback">
                        {{ $errors->first('bank_account') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.payout.fields.bank_account_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="amount">{{ trans('cruds.payout.fields.amount') }}</label>
                <input class="form-control {{ $errors->has('amount') ? 'is-invalid' : '' }}" type="number" name="amount" id="amount" value="{{ old('amount', '') }}" step="0.01" required>
                @if($errors->has('amount'))
                    <div class="invalid-feedback">
                        {{ $errors->first('amount') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.payout.fields.amount_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.payout.fields.status') }}</label>
                @foreach(App\Models\Payout::STATUS_RADIO as $key => $label)
                    <div class="form-check {{ $errors->has('status') ? 'is-invalid' : '' }}">
                        <input class="form-check-input" type="radio" id="status_{{ $key }}" name="status" value="{{ $key }}" {{ old('status', 'pending') === (string) $key ? 'checked' : '' }} required>
                        <label class="form-check-label" for="status_{{ $key }}">{{ $label }}</label>
                    </div>
                @endforeach
                @if($errors->has('status'))
                    <div class="invalid-feedback">
                        {{ $errors->first('status') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.payout.fields.status_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="exported_at">{{ trans('cruds.payout.fields.exported_at') }}</label>
                <input class="form-control timepicker {{ $errors->has('exported_at') ? 'is-invalid' : '' }}" type="text" name="exported_at" id="exported_at" value="{{ old('exported_at') }}">
                @if($errors->has('exported_at'))
                    <div class="invalid-feedback">
                        {{ $errors->first('exported_at') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.payout.fields.exported_at_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection